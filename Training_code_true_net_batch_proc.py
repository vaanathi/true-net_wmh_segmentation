#!/usr/bin/env python
#   Copyright (C) 2016 University of Oxford 
#   SHBASECOPYRIGHT

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
import pickle as pkl
from sklearn.manifold import TSNE
from utils import *
from scipy.ndimage import filters
import math
from skimage import exposure
import augmentations_gmdist
import augmentations_distmaps2, augmentations_distmaps, augmentations_distmaps_t1
from skimage.transform import resize
import data_preprocessing_functions
import model_layers_functions 
import loss_functions 
import data_functions
import glob
from nwei_unet_2dt1patches_model_miccai_3planes import UnetFcnAxialModel, UnetFcnSagittalModel, UnetFcnCoronalModel


def evaluate(testdata,graph, model, sess, num_steps=2, verbose=False):
    """Helper to run the model with different training modes."""
    # Batch generators
    batch_size = 8
    prob_array = np.array([])
    mask_array = np.array([])
    for d in range(len(testdata)):
        da = testdata[d]
        if len(da.shape) == 4:
            extra = np.zeros([8,da.shape[1],da.shape[2],da.shape[3]])
        else:
            extra = np.zeros([8,da.shape[1],da.shape[2]])
        da = np.concatenate([da,extra],axis=0)
        testdata[d] = da
    gen_batch = batch_generator(testdata, batch_size, shuffle = False)
    num_steps = testdata[0].shape[0]//batch_size
    for i in range(num_steps):
        X, y, pwg, pwv = next(gen_batch)
        pix_weights_gm = pwg
        pix_weights_vent = pwv
        pix_weights = (pix_weights_gm + pix_weights_vent) * (pix_weights_vent > 0).astype(float)
        # Compute final evaluation on test data
        accuracy,probs,mask_im = sess.run([label_acc_p,prob_values_p,wmh_mask_p],
                        feed_dict={model.X: X, model.classify_labels: y,
                                   model.train: False, model.pixel_weights: pix_weights})
        prob_array = np.concatenate((prob_array,probs),axis=0) if prob_array.size else probs
        mask_array = np.concatenate((mask_array,mask_im),axis=0) if mask_array.size else mask_im

    return accuracy, prob_array, mask_array

def reset_session(sess):
    
    sess.close()
    sess =  tf.Session(graph=graph)
    tf.global_variables_initializer().run(session=sess)
    return sess


def train_and_evaluate(train_names, val_names, mode, graph, model, sess, num_steps=3000, verbose=False):
    """Helper to run the model with different training modes."""
    batch_size = 8
    batch_factor = 10 # determines how many images are loaded for training at an iteration
    num_iters = len(train_names)//batch_factor
    losses = []
    losses_val = []
    for i in range(num_iters):
        trainnames = train_names[i*batch_factor:(i+1)*batch_factor]
        brains, data, data_t1, labels, GM_distance, ventdistmap = data_functions.create_data_array_from_loaded_data(trainnames, plane=mode)
        traindata = data_functions.get_slices_from_data_with_aug(brains,data,data_t1,labels,GM_distance,ventdistmap)
        brains, data, data_t1, labels, GM_distance, ventdistmap = data_functions.create_data_array_from_loaded_data(val_names, plane=mode)
        valdata = data_functions.get_slices_from_data_with_aug(brains,data,data_t1,labels,GM_distance,ventdistmap)
        num_steps = traindata[0].shape[0]*20
    
        gen_next_train_batch = batch_generator(traindata, batch_size, shuffle=True)
        gen_next_val_batch = batch_generator(valdata, batch_size, shuffle=False)
        saver = tf.train.Saver()
            
        # Training loop
        learng_rate = np.hstack([np.hstack([0.001*np.ones([1,50]),0.0002*np.ones([1,60])]),0.0001*np.ones([1,6200])])
        learng_rate = learng_rate.astype(float)    
        for i in range(num_steps):
            p = float(i) / num_steps
            l = 2. / (1. + np.exp(-10. * p)) - 1
            lr = learng_rate[0][i]
            print(lr.dtype)
            X, y, pwg, pwv = next(gen_next_train_batch)
            pix_weights_gm = pwg
            pix_weights_vent = pwv
            pix_weights = (pix_weights_gm + pix_weights_vent) * (pix_weights_vent > 0).astype(float)
            _, batch_loss,accur = sess.run([train_op_p, loss_op_p, label_acc_p],
                                     feed_dict={model.X: X, model.classify_labels: y, model.train: True,
                                                model.learning_rate: lr, model.pixel_weights: pix_weights})
            print('For epoch {}: The loss value is {} and the accuracy is {}'.format(i, batch_loss, accur))
            losses0.append(batch_loss[0])
            losses1.append(batch_loss[1])
            if i%5 == 0:
                Xv, yv, pwgv, pwvv = next(gen_next_val_batch)
                pix_weights_gm = pwgv
                pix_weights_vent = pwvv
                pix_weights = (pix_weights_gm + pix_weights_vent) * (pix_weights_vent > 0).astype(float)
                # Compute final evaluation on test data
                loss_val,accuracy,probs,mask_im = sess.run([loss_op_p,label_acc_p,prob_values_p,wmh_mask_p],
                        feed_dict={model.X: Xv, model.classify_labels: yv,
                                   model.train: False, model.pixel_weights: pix_weights})
                losses_val0.append(loss_val[0])
                losses_val1.append(loss_val[1])
            if i%(traindata[0].shape[0]) == 0:
                losses.append(sum(mean(losses0),mean(losses1)))
                losses_val.append(sum(mean(losses_val0),mean(losses_val1)))
                losses0 = []
                losses1 = []
                losses_val0 = []
                losses_val1 = []
    os.mkdir('./trained_truenet_model_'+mode)
    saver.save(sess, './trained_truenet_model_'+mode+'/trained_truenet_ep20_' + mode, global_step=num_steps*)
    return losses, losses_val


# Load training data names
train_folder = '/training/folder/name'
train_names = glob.glob(train_folder+'*.nii.gz')
val_folder = '/validation/folder/name'
val_names = glob.glob(val_folder+'*.nii.gz')

# Axial plane
tf.reset_default_graph() 
graph = tf.get_default_graph()
with graph.as_default():
    mod_p = UnetFcnAxialModel()
    loss_p = tf.reduce_mean(mod_p.loss)
    loss0_p = tf.reduce_mean(mod_p.loss0)
    loss1_p = tf.reduce_mean(mod_p.loss1)
    train_op_p = tf.train.AdamOptimizer(mod_p.learning_rate,epsilon=1e-04).minimize(loss_p)
    loss_op_p = [loss0_p, loss1_p]
    prob_values_p = mod_p.pred
    wmh_mask_p = mod_p.pred_mask
    Logits_p = mod_p.logits
    correct_label_pred_p = tf.equal(tf.cast(mod_p.classify_labels, tf.float32), tf.cast(mod_p.pred_mask, tf.float32))
    label_acc_p = tf.reduce_mean(tf.cast(correct_label_pred_p, tf.float32))
config = tf.ConfigProto() 
config.gpu_options.allow_growth = True
sess1 =  tf.Session(graph=graph, config=config)
tf.global_variables_initializer().run(session=sess1)
lsp_axial, lsvp_axial = train_and_evaluate(train_names, val_names, 'axial', graph, mod_p, sess1)

# Sagittal plane
tf.reset_default_graph() 
graph = tf.get_default_graph()
with graph.as_default():
    mod_p = UnetFcnSagittalModel()
    loss_p = tf.reduce_mean(mod_p.loss)
    loss0_p = tf.reduce_mean(mod_p.loss0)
    loss1_p = tf.reduce_mean(mod_p.loss1)
    train_op_p = tf.train.AdamOptimizer(mod_p.learning_rate,epsilon=1e-04).minimize(loss_p)
    loss_op_p = [loss0_p, loss1_p]
    prob_values_p = mod_p.pred
    wmh_mask_p = mod_p.pred_mask
    Logits_p = mod_p.logits
    correct_label_pred_p = tf.equal(tf.cast(mod_p.classify_labels, tf.float32), tf.cast(mod_p.pred_mask, tf.float32))
    label_acc_p = tf.reduce_mean(tf.cast(correct_label_pred_p, tf.float32))
config = tf.ConfigProto() 
config.gpu_options.allow_growth = True
sess1 =  tf.Session(graph=graph, config=config)
tf.global_variables_initializer().run(session=sess1)
lsp_sag, lsvp_sag = train_and_evaluate(train_names, val_names, 'sagittal', graph, mod_p, sess1)

# Coronal plane
tf.reset_default_graph() 
graph = tf.get_default_graph()
with graph.as_default():
    mod_p = UnetFcnCoronalModel()
    loss_p = tf.reduce_mean(mod_p.loss)
    loss0_p = tf.reduce_mean(mod_p.loss0)
    loss1_p = tf.reduce_mean(mod_p.loss1)
    train_op_p = tf.train.AdamOptimizer(mod_p.learning_rate,epsilon=1e-04).minimize(loss_p)
    loss_op_p = [loss0_p, loss1_p]
    prob_values_p = mod_p.pred
    wmh_mask_p = mod_p.pred_mask
    Logits_p = mod_p.logits
    correct_label_pred_p = tf.equal(tf.cast(mod_p.classify_labels, tf.float32), tf.cast(mod_p.pred_mask, tf.float32))
    label_acc_p = tf.reduce_mean(tf.cast(correct_label_pred_p, tf.float32))
config = tf.ConfigProto() 
config.gpu_options.allow_growth = True
sess1 =  tf.Session(graph=graph, config=config)
tf.global_variables_initializer().run(session=sess1)
lsp_cor, lsvp_cor = train_and_evaluate(train_names, val_names, 'coronal', graph, mod_p, sess1)


