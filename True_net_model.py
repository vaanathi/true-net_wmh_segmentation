#!/usr/bin/env python
#   Copyright (C) 2016 University of Oxford 
#   SHBASECOPYRIGHT

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
import pickle as pkl
from sklearn.manifold import TSNE
from utils import *
from scipy.ndimage import filters
import math
from skimage import exposure
import augmentations_gmdist
import augmentations_distmaps2, augmentations_distmaps
from skimage.transform import resize
import model_layers_functions 
import loss_functions 

class UnetFcnAxialModel(object):
    def __init__(self):
        self.batch_size = 8
        self._build_model()

    def _build_model(self):        
        self.sce_weight = 1
        self.train = tf.placeholder(tf.bool,[],name='train_flag')
        self.learning_rate = tf.placeholder(tf.float32, [], name='lrate')
        self.X = tf.placeholder(tf.float32, [self.batch_size, 128, 192, 2],name='x')
        self.classify_labels = tf.placeholder(tf.float32, [self.batch_size, 128, 192],name='y')
        self.pixel_weights = tf.placeholder(tf.float32, [self.batch_size, 128, 192],name='pix_wei')
        
        num_classes = 2  # Number of classes
        init_channels = 64  # Number of channels in the first conv layer
        n_layers = 3  # Number of times to downsample/upsample
        batch_norm = True  # if True, use batch-norm
        is_training = True

        # color-space adjustment
        feed = tf.layers.conv2d(self.X, 3, (1, 1))
        n = n_layers

        ch = init_channels
        kers = [[3,3],[3,3],[3,3],[3,3]]# ,[3,1]]
        conv_blocks = []
        for i in range(n_layers):
            # feed = model_layers_functions.add_coordconv_module(feed,0)
            conv, feed = model_layers_functions.conv_module2d(feed, ch, kers[i], is_training, name='down_patches{}'.format(i + 1),
                                     batch_norm=batch_norm)
            conv_blocks.append(conv)
            ch *= 2
        # feed = model_layers_functions.add_coordconv_module(feed,0)
        last_conv = model_layers_functions.conv_module2d(feed, ch, kers[-1], is_training, name='down_patches{}'.format(n_layers+1),
                                pool=False, batch_norm=batch_norm)
        conv_blocks.append(last_conv)

        # decoder / upsampling
        feed = conv_blocks[-1]
        kers = [[3,3],[3,3],[3,3],[3,3]]# ,[3,1]]
        for i in range(n_layers, 0, -1):
            up = model_layers_functions.deconv_correct_module_halfch2d(feed, name='deconv_patches'+str(i+1), stride=2, kernel_size=3)
            concat = tf.concat([up, conv_blocks[i-1]], axis=-1, name="concat_patches{}".format(i))
            feed = model_layers_functions.conv_module2d(concat, ch, kers[i-1], is_training, name='up_patches{}'.format(i), batch_norm=batch_norm,
                               pool=False)
            ch /= 2
            
        # self.logits = tf.layers.conv2d(feed, num_classes, (3,3), name='pre_logits_patches', activation=tf.nn.relu, padding='same')
        self.logits = tf.layers.conv2d(feed, num_classes, (1,1), name='logits_patches', activation=None, padding='same')
        # self.logits = tf.concat([fin_layer,fin_layer], axis=-1, name="final_logits")
        probs, tf_mask = loss_functions.mask_prediction2d(self.logits)
        self.pred = probs
        
        tf_mask = tf_mask#  * tf_data1
        self.pred_mask = tf_mask
        self.loss0 = loss_functions.dice_coef_loss2d(self.classify_labels,tf_mask)
        self.loss1 = loss_functions.pixel_wise_loss(self.logits,self.classify_labels,pixel_weights=self.pixel_weights)
        self.loss = self.loss0 + self.sce_weight * self.loss1
        
class UnetFcnSagittalModel(object):
    def __init__(self):
        self.batch_size = 8
        self._build_model()

    def _build_model(self):        
        self.sce_weight = 1
        self.train = tf.placeholder(tf.bool,[],name='train_flag')
        self.learning_rate = tf.placeholder(tf.float32, [],name='lrate')
        self.X = tf.placeholder(tf.float32, [self.batch_size, 192, 120, 2],name='x')
        self.classify_labels = tf.placeholder(tf.float32, [self.batch_size, 192, 120],name='y')
        self.pixel_weights = tf.placeholder(tf.float32, [self.batch_size, 192, 120],name='pix_wei')
        
        num_classes = 2  # Number of classes
        init_channels = 64  # Number of channels in the first conv layer
        n_layers = 3  # Number of times to downsample/upsample
        batch_norm = True  # if True, use batch-norm
        is_training = True

        # color-space adjustment
        feed = tf.layers.conv2d(self.X, 3, (1, 1))
        n = n_layers

        ch = init_channels
        kers = [[5,5],[3,3],[3,3],[3,3]]# ,[3,1]]
        conv_blocks = []
        for i in range(n_layers):
            # feed = model_layers_functions.add_coordconv_module(feed,0)
            conv, feed = model_layers_functions.conv_module2d(feed, ch, kers[i], is_training, name='down_patches{}'.format(i + 1),
                                     batch_norm=batch_norm)
            conv_blocks.append(conv)
            ch *= 2
        # feed = model_layers_functions.add_coordconv_module(feed,0)
        last_conv = model_layers_functions.conv_module2d(feed, ch, kers[-1], is_training, name='down_patches{}'.format(n_layers+1),
                                pool=False, batch_norm=batch_norm)
        conv_blocks.append(last_conv)

        # decoder / upsampling
        feed = conv_blocks[-1]
        kers = [[3,3],[3,3],[3,3],[3,3]]# ,[3,1]]
        for i in range(n_layers, 0, -1):
            up = model_layers_functions.deconv_correct_module_halfch2d(feed, name='deconv_patches'+str(i+1), stride=2, kernel_size=3)
            concat = tf.concat([up, conv_blocks[i-1]], axis=-1, name="concat_patches{}".format(i))
            feed = model_layers_functions.conv_module2d(concat, ch, kers[i-1], is_training, name='up_patches{}'.format(i), batch_norm=batch_norm,
                               pool=False)
            ch /= 2
            
        # self.logits = tf.layers.conv2d(feed, num_classes, (3,3), name='pre_logits_patches', activation=tf.nn.relu, padding='same')
        self.logits = tf.layers.conv2d(feed, num_classes, (1,1), name='logits_patches', activation=None, padding='same')
        # self.logits = tf.concat([fin_layer,fin_layer], axis=-1, name="final_logits")
        probs, tf_mask = loss_functions.mask_prediction2d(self.logits)
        self.pred = probs
        
        tf_mask = tf_mask#  * tf_data1
        self.pred_mask = tf_mask
        
        self.loss0 = loss_functions.dice_coef_loss2d(self.classify_labels,tf_mask)
        self.loss1 = loss_functions.pixel_wise_loss(self.logits,self.classify_labels,pixel_weights=self.pixel_weights)
        self.loss = self.loss0 + self.sce_weight * self.loss1

class UnetFcnCoronalModel(object):
    def __init__(self):
        self.batch_size = 8
        self._build_model()

    def _build_model(self):        
        self.sce_weight = 1
        self.train = tf.placeholder(tf.bool,[],name='train_flag')
        self.learning_rate = tf.placeholder(tf.float32, [],name='lrate')
        self.X = tf.placeholder(tf.float32, [self.batch_size, 128, 80, 2],name='x')
        self.classify_labels = tf.placeholder(tf.float32, [self.batch_size, 128, 80],name='y')
        self.pixel_weights = tf.placeholder(tf.float32, [self.batch_size, 128, 80],name='pix_wei')
        
        num_classes = 2  # Number of classes
        init_channels = 64  # Number of channels in the first conv layer
        n_layers = 3  # Number of times to downsample/upsample
        batch_norm = True  # if True, use batch-norm
        is_training = True

        # color-space adjustment
        feed = tf.layers.conv2d(self.X, 3, (1, 1))
        n = n_layers

        ch = init_channels
        kers = [[5,5],[3,3],[3,3],[3,3]]# ,[3,1]]
        conv_blocks = []
        for i in range(n_layers):
            # feed = model_layers_functions.add_coordconv_module(feed,0)
            conv, feed = model_layers_functions.conv_module2d(feed, ch, kers[i], is_training, name='down_patches{}'.format(i + 1),
                                     batch_norm=batch_norm)
            conv_blocks.append(conv)
            ch *= 2
        # feed = model_layers_functions.add_coordconv_module(feed,0)
        last_conv = model_layers_functions.conv_module2d(feed, ch, kers[-1], is_training, name='down_patches{}'.format(n_layers+1),
                                pool=False, batch_norm=batch_norm)
        conv_blocks.append(last_conv)

        # decoder / upsampling
        feed = conv_blocks[-1]
        kers = [[3,3],[3,3],[3,3],[3,3]]# ,[3,1]]
        for i in range(n_layers, 0, -1):
            up = model_layers_functions.deconv_correct_module_halfch2d(feed, name='deconv_patches'+str(i+1), stride=2, kernel_size=3)
            concat = tf.concat([up, conv_blocks[i-1]], axis=-1, name="concat_patches{}".format(i))
            feed = model_layers_functions.conv_module2d(concat, ch, kers[i-1], is_training, name='up_patches{}'.format(i), batch_norm=batch_norm,
                               pool=False)
            ch /= 2
            
        # self.logits = tf.layers.conv2d(feed, num_classes, (3,3), name='pre_logits_patches', activation=tf.nn.relu, padding='same')
        self.logits = tf.layers.conv2d(feed, num_classes, (1,1), name='logits_patches', activation=None, padding='same')
        # self.logits = tf.concat([fin_layer,fin_layer], axis=-1, name="final_logits")
        probs, tf_mask = loss_functions.mask_prediction2d(self.logits)
        self.pred = probs
        
        tf_mask = tf_mask#  * tf_data1
        self.pred_mask = tf_mask
        
        self.loss0 = loss_functions.dice_coef_loss2d(self.classify_labels,tf_mask)
        self.loss1 = loss_functions.pixel_wise_loss(self.logits,self.classify_labels,pixel_weights=self.pixel_weights)
        self.loss = self.loss0 + self.sce_weight * self.loss1
