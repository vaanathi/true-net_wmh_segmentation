#!/usr/bin/env python
#   Copyright (C) 2016 University of Oxford 
#   SHBASECOPYRIGHT

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
import pickle as pkl
from sklearn.manifold import TSNE
from utils import *
from scipy.ndimage import filters
import math
from skimage import exposure
import augmentations_gmdist
import augmentations_distmaps2, augmentations_distmaps
from skimage.transform import resize

def preprocess_data(data):    
    return (2 * data / np.amax(np.reshape(data,[-1,1]))) - 1

def preprocess_data_gauss(data):
    brain = (data > 10).astype(float)
    data = data - np.mean(data[brain == 1])      
    data = data/np.std(data[brain == 1])
    return data

def rescale_its_intensity(image):
    min_limit = 50
    v_min, v_max = np.percentile(image, (min_limit, 100))
    new_img = exposure.rescale_intensity(image, in_range=(v_min, v_max))    
    return new_img

def cut_zeros1d(im_array):
    im_list = list(im_array > 0)
    start_index = im_list.index(1)
    end_index = im_list[::-1].index(1)
    length = len(im_array[start_index:])-end_index
    return start_index, end_index, length

def tight_crop_data(img_data):
    row_sum = np.sum(np.sum(img_data,axis=1),axis=1)
    col_sum = np.sum(np.sum(img_data,axis=0),axis=1)
    stack_sum = np.sum(np.sum(img_data,axis=1),axis=0)
    rsid, reid, rlen = cut_zeros1d(row_sum)
    csid, ceid, clen = cut_zeros1d(col_sum)
    ssid, seid, slen = cut_zeros1d(stack_sum)
    return img_data[rsid:rsid+rlen, csid:csid+clen, ssid:ssid+slen], [rsid, rlen, csid, clen, ssid, slen]

def hist_match(imsrc):
    imres = imsrc.copy()
    nbr_bins = 255
    imhist,bins = np.histogram(imsrc[imsrc>0].flatten(),nbr_bins,normed=True)
    cdfsrc = imhist.cumsum() #cumulative distribution function
    cdfsrc = (255 * cdfsrc / cdfsrc[-1]).astype(np.uint8) #normalize
    cdfref = np.load('/gpfs2/well/win/users/tjj573/WMH_Seg/Unet_tf/SW_Unets/ref_cdf.npy')
    im2 = np.interp(imsrc[imsrc>0].flatten(),bins[:-1],cdfsrc)
    im3 = np.interp(im2,cdfref, bins[:-1])
    imres[imsrc>0] = im3
    return imres

def anisodiff(img,niter=1,kappa=50,gamma=0.1,step=(1.,1.),option=1,ploton=False):
    # ...you could always diffuse each color channel independently if you
    # really want
    if img.ndim == 3:
            warnings.warn("Only grayscale images allowed, converting to 2D matrix")
            img = img.mean(2)

    # initialize output array
    img = img.astype('float32')
    imgout = img.copy()

    # initialize some internal variables
    deltaS = np.zeros_like(imgout)
    deltaE = deltaS.copy()
    NS = deltaS.copy()
    EW = deltaS.copy()
    gS = np.ones_like(imgout)
    gE = gS.copy()

    # create the plot figure, if requested
    if ploton:
            import pylab as pl
            from time import sleep

            fig = pl.figure(figsize=(20,5.5),num="Anisotropic diffusion")
            ax1,ax2 = fig.add_subplot(1,2,1),fig.add_subplot(1,2,2)

            ax1.imshow(img,interpolation='nearest')
            ih = ax2.imshow(imgout,interpolation='nearest',animated=True)
            ax1.set_title("Original image")
            ax2.set_title("Iteration 0")

            fig.canvas.draw()

    for ii in range(niter):

            # calculate the diffs
            deltaS[:-1,: ] = np.diff(imgout,axis=0)
            deltaE[: ,:-1] = np.diff(imgout,axis=1)

            # conduction gradients (only need to compute one per dim!)
            if option == 1:
                    gS = np.exp(-(deltaS/kappa)**2.)/step[0]
                    gE = np.exp(-(deltaE/kappa)**2.)/step[1]
            elif option == 2:
                    gS = 1./(1.+(deltaS/kappa)**2.)/step[0]
                    gE = 1./(1.+(deltaE/kappa)**2.)/step[1]

            # update matrices
            E = gE*deltaE
            S = gS*deltaS

            # subtract a copy that has been shifted 'North/West' by one
            # pixel. don't as questions. just do it. trust me.
            NS[:] = S
            EW[:] = E
            NS[1:,:] -= S[:-1,:]
            EW[:,1:] -= E[:,:-1]

            # update the image
            imgout += gamma*(NS+EW)

            if ploton:
                    iterstring = "Iteration %i" %(ii+1)
                    ih.set_data(imgout)
                    ax2.set_title(iterstring)
                    fig.canvas.draw()
                    # sleep(0.01)

    return imgout
