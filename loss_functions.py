#!/usr/bin/env python
#   Copyright (C) 2016 University of Oxford 
#   SHBASECOPYRIGHT

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
import pickle as pkl
from sklearn.manifold import TSNE
from utils import *
from scipy.ndimage import filters
import math
from skimage import exposure
import augmentations_gmdist
import augmentations_distmaps2, augmentations_distmaps
from skimage.transform import resize

# Loss functions
def dice_coef(y_true, y_pred, axis=None, smooth=1):
    if axis is None:
        axis=[1,2,3]
    y_true_f = tf.cast(y_true, dtype=tf.float32)
    y_pred_f = tf.cast(y_pred, dtype=tf.float32)
    intersection = tf.reduce_sum(y_true_f * y_pred_f, axis=axis)
    dice = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f, axis=axis)
                                               + tf.reduce_sum(y_pred_f, axis=axis) + smooth)
    return tf.reduce_mean(dice)

def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)


def weighted_dice_coef(y_true, y_pred, axis=None, smooth=1):
    y_pred = tf.cast(y_pred, dtype=tf.float32)
    if axis is None:
        axis=[1,2,3]
    y_true2 = tf.cast(y_true > 1, dtype=tf.float32)
    y_pred2 = y_pred * y_true2


    y_true_f = tf.cast(y_true2, dtype=tf.float32)
    y_pred_f = tf.cast(y_pred2, dtype=tf.float32)
    intersection = tf.reduce_sum(y_true_f * y_pred_f, axis=axis)
    dice2 = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f, axis=axis)
                                               + tf.reduce_sum(y_pred_f, axis=axis) + smooth)
    if tf.reduce_sum(y_true2, axis=axis) == 0:
        dice2 = 0
    y2 = tf.cast(y_true < 2 , dtype=tf.float32)
    y0 = tf.cast(y_true > 0 , dtype=tf.float32)  
    y_true1 = y2 * y0
    y_true2 = tf.cast(y_true1, dtype=tf.float32)
    y_pred2 = y_pred * y_true2

    y_true_f = tf.cast(y_true2, dtype=tf.float32)
    y_pred_f = tf.cast(y_pred2, dtype=tf.float32)
    intersection = tf.reduce_sum(y_true_f * y_pred_f, axis=axis)
    dice = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f, axis=axis)
                                               + tf.reduce_sum(y_pred_f, axis=axis) + smooth)
    out = tf.cast(tf.reduce_mean(dice), dtype=tf.float32) 
    out1 = tf.cast(tf.reduce_mean(dice2), dtype=tf.float32)
    return (out + out1)/2

def weighted_dice_coef_loss(y_true, y_pred):
    return -weighted_dice_coef(y_true, y_pred)

def pixel_wise_loss(pixel_logits, gt_pixels, pixel_weights=None):
    """Calculates pixel-wise softmax cross entropy loss
    Args:
        pixel_logits (4-D Tensor): (N, H, W, 2)
        gt_pixels (3-D Tensor): Image masks of shape (N, H, W, 2)
        pixel_weights (3-D Tensor) : (N, H, W) Weights for each pixel
    Returns:
        scalar loss : softmax cross-entropy
    """
    logits = tf.reshape(pixel_logits, [-1, 2])
    logitsf = tf.cast(logits, dtype=tf.float32)
    labels = tf.reshape(gt_pixels, [-1, 1])
    labels = tf.stack([1-labels,labels], axis=1)
    labels = tf.reshape(labels,[-1,2])
    loss = tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=labels)
    if pixel_weights is None:
        return tf.reduce_mean(loss)
    else:
        weights = tf.reshape(pixel_weights, [-1])
        return tf.reduce_sum(loss * weights) / tf.reduce_sum(weights)


def mask_prediction(pixel_logits):
    """
    Args:
        pixel_logits (4-D Tensor): (N, H, W, 2)
    Returns:
        Predicted pixel-wise probabilities (3-D Tensor): (N, H, W)
        Predicted mask (3-D Tensor): (N, H, W)
    """
    probs = tf.nn.softmax(pixel_logits)
    n, d, h, w, _ = probs.get_shape()
    masks = tf.reshape(probs, [-1, 2])
    masks = tf.cast(masks[:,1] > 0.5, tf.int64)#f.argmax(masks, axis=1)
    masks = tf.reshape(masks, [n.value, d.value, h.value, w.value])
#     probs = tf.slice(probs, [0, 0, 0, 0], [-1, -1, -1, 1])
#     probs = tf.squeeze(probs, axis=-1)
    return probs, masks
