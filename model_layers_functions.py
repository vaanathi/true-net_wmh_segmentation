#!/usr/bin/env python
#   Copyright (C) 2016 University of Oxford 
#   SHBASECOPYRIGHT

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
import pickle as pkl
from sklearn.manifold import TSNE
from utils import *
from scipy.ndimage import filters
import math
from skimage import exposure
import augmentations_gmdist
import augmentations_distmaps2, augmentations_distmaps
from skimage.transform import resize

def conv_module(input_, n_filters, kernel_sizes, training, name, pool=True, activation=tf.nn.relu,
                padding='same', batch_norm=True):
    """{Conv -> BN -> RELU} x 2 -> {Pool, optional}
        reference : https://github.com/kkweon/UNet-in-Tensorflow
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        n_filters (int): depth of output tensor
        training (bool): If True, run in training mode
        name (str): name postfix
        pool (bool): If True, MaxPool2D after last conv layer
        activation: Activaion functions
        padding (str): 'same' or 'valid'
        batch_norm (bool) : If True, use batch-norm
    Returns:
        u_net: output of the Convolution operations
        pool (optional): output of the max pooling operations
    """
    #kernel_sizes = [1,3,3,1]
    net = input_
    with tf.variable_scope("conv_module_{}".format(name)):
        for i, k_size in enumerate(kernel_sizes):
            net = tf.layers.conv3d(net, n_filters, (k_size, k_size, k_size), activation=None, padding=padding,
                                   name="conv_{}".format(i + 1))

            if batch_norm:
                net = tf.layers.batch_normalization(net, training=training, renorm=True,
                                                    name="bn_{}".format(i + 1))
            net = activation(net, name="relu_{}".format(i + 1))

        if pool is False:
            return net

        pool = tf.layers.max_pooling3d(net, (2, 2, 2), strides=(2, 2, 2), name="pool")

        return net, pool

def conv_module2d(input_, n_filters, kernel_sizes, training, name, pool=True, activation=tf.nn.relu,
                padding='same', batch_norm=True):
    """{Conv -> BN -> RELU} x 2 -> {Pool, optional}
        reference : https://github.com/kkweon/UNet-in-Tensorflow
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        n_filters (int): depth of output tensor
        training (bool): If True, run in training mode
        name (str): name postfix
        pool (bool): If True, MaxPool2D after last conv layer
        activation: Activaion functions
        padding (str): 'same' or 'valid'
        batch_norm (bool) : If True, use batch-norm
    Returns:
        u_net: output of the Convolution operations
        pool (optional): output of the max pooling operations
    """
    #kernel_sizes = [1,3,3,1]
    net = input_
    with tf.variable_scope("conv_module_{}".format(name)):
        for i, k_size in enumerate(kernel_sizes):
            net = tf.layers.conv2d(net, n_filters, (k_size, k_size), activation=None, padding=padding,
                                   name="conv_{}_{}".format(name,i + 1))

            if batch_norm:
                net = tf.layers.batch_normalization(net, training=training, renorm=True,
                                                    name="bn_{}".format(i + 1))
            net = activation(net, name="relu_{}_{}".format(name,i + 1))

        if pool is False:
            return net

        pool = tf.layers.max_pooling2d(net, (2, 2), strides=(2, 2), name="pool")

        return net, pool


def global_conv_module(input_, num_classes, training, name, k=13, padding='same'):
    """Global convolution network [https://arxiv.org/abs/1703.02719]
    Args:
         input_ (4-D Tensor): (batch_size, H, W, C)
         num_classes (integer) : Number of classes to classify
         name (str): name postfix
         k (integer): filter size for 1 x k + k x 1 convolutions
         padding (str) : 'same' or 'valid
    Returns:
         net (4-D Tensor): (batch_size, H, W, num_classes)
    """
    net = input_
    n_filters = num_classes

    with tf.variable_scope("global_conv_module_{}".format(name)):
        branch_a = tf.layers.conv3d(net, n_filters, (k, 1, 1), activation=None,
                                     padding=padding, name='conv_1a')
        branch_a = tf.layers.conv3d(branch_a, n_filters, (1, 1, k), activation=None,
                                     padding=padding, name='conv_2a')
        branch_a = tf.layers.conv3d(branch_a, n_filters, (1, k, 1), activation=None,
                                     padding=padding, name='conv_3a')

        branch_b = tf.layers.conv3d(net, n_filters, (1, k, 1), activation=None,
                                     padding=padding, name='conv_1b')
        branch_b = tf.layers.conv3d(branch_b, n_filters, (1, 1, k), activation=None,
                                     padding=padding, name='conv_2b')
        branch_b = tf.layers.conv3d(branch_b, n_filters, (k, 1, 1), activation=None,
                                     padding=padding, name='conv_3b')

        net = tf.add(branch_a, branch_b, name='sum')

        return net


def boundary_refine(input_, training, name, activation=tf.nn.relu, batch_norm=True):
    """Boundary refinement network [https://arxiv.org/abs/1703.02719]
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        training (bool): If True, run in training mode
        name (str): name postfix
        activation: Activaion functions
        batch_norm (bool) : Whether to use batch norm
    Returns:
        net (4-D Tensor): output tensor of same shape as input_
    """
    net = input_
    n_filters = input_.get_shape()[4].value

    with tf.variable_scope("boundary_refine_module_{}".format(name)):

        net = tf.layers.conv3d(net, n_filters, (1, 1, 1), activation=None,
                               padding='SAME', name='conv_1')
        if batch_norm:
            net = tf.layers.batch_normalization(net, training=training,
                                                name='bn_1', renorm=True)
        net = activation(net, name='relu_1')

        net = tf.layers.conv3d(net, n_filters, (1, 1, 1), activation=None,
                               padding='SAME', name='conv_2')
        net = tf.add(net, input_, name='sum')

        return net


def boundary_refine2d(input_, training, name, activation=tf.nn.relu, batch_norm=True):
    """Boundary refinement network [https://arxiv.org/abs/1703.02719]
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        training (bool): If True, run in training mode
        name (str): name postfix
        activation: Activaion functions
        batch_norm (bool) : Whether to use batch norm
    Returns:
        net (4-D Tensor): output tensor of same shape as input_
    """
    net = input_
    n_filters = input_.get_shape()[3].value

    with tf.variable_scope("boundary_refine_module_{}".format(name)):

        net = tf.layers.conv2d(net, n_filters, (1, 1), activation=None,
                               padding='SAME', name='conv_1')
        if batch_norm:
            net = tf.layers.batch_normalization(net, training=training,
                                                name='bn_1', renorm=True)
        net = activation(net, name='relu_1')

        net = tf.layers.conv2d(net, n_filters, (1, 1), activation=None,
                               padding='SAME', name='conv_2')
        net = tf.add(net, input_, name='sum')

        return net

def get_deconv_filter(name, n_channels, k_size):
    """Creates weight kernel initialization for deconvolution layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        name (str): name postfix
        n_channels (int): number of input and output channels are same
        k_size (int): kernel-size (~ 2 x stride for FCN case)
    Returns:
        weight kernels (4-D Tensor): (k_size , k_size, n_channels, n_channels)
    """
    k = k_size
    filter_shape = [k, k, k, n_channels, n_channels]
    f = math.ceil(k / 2.0)
    c = (2 * f - 1 - f % 2) / (2.0 * f)
    bilinear = np.zeros((k, k, k))
    for x, y, z in zip(range(k), range(k), range(k)):
        bilinear[x, y, z] = (1 - abs(x / f - c)) * (1 - abs(y / f - c))
    weights = np.zeros(filter_shape)
    for i in range(n_channels):
        weights[:, :, :, i, i] = bilinear

    init = tf.constant_initializer(value=weights, dtype=tf.float32)
    var = tf.get_variable(name=name, initializer=init, shape=weights.shape)
    return var

def get_correct_deconv_filter(name, n_channels, k_size):
    """Creates weight kernel initialization for deconvolution layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        name (str): name postfix
        n_channels (int): number of input and output channels are same
        k_size (int): kernel-size (~ 2 x stride for FCN case)
    Returns:
        weight kernels (4-D Tensor): (k_size , k_size, k_size, n_channels, n_channels)
    """
    k = k_size
    f = math.floor((k+1)/2)
    if k%2 == 1:
        cent = f
    else:
        cent = f + 0.5
    C = np.arange(1,k+1)
    
    filter_shape = [k, k, k, n_channels, n_channels]
    weights = np.zeros(filter_shape)
    for i, j in zip(range(int(n_channels)), range(int(n_channels))):
        weights[:, :, :, i, j] = np.transpose(np.ones([1,k]) - abs(C-cent)/f)*(np.ones([1,k]) - abs(C-cent)/(f))

    init = tf.constant_initializer(value=weights, dtype=tf.float32)
    var = tf.get_variable(name=name, initializer=init, shape=weights.shape)
    return var

def get_correct_deconv_filter2d(name, n_channels, k_size):
    """Creates weight kernel initialization for deconvolution layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        name (str): name postfix
        n_channels (int): number of input and output channels are same
        k_size (int): kernel-size (~ 2 x stride for FCN case)
    Returns:
        weight kernels (4-D Tensor): (k_size , k_size, k_size, n_channels, n_channels)
    """
    k = k_size
    f = math.floor((k+1)/2)
    if k%2 == 1:
        cent = f
    else:
        cent = f + 0.5
    C = np.arange(1,k+1)
    
    filter_shape = [k, k, n_channels, n_channels]
    weights = np.zeros(filter_shape)
    for i, j in zip(range(int(n_channels)), range(int(n_channels))):
        weights[:, :, i, j] = np.transpose(np.ones([1,k]) - abs(C-cent)/f)*(np.ones([1,k]) - abs(C-cent)/(f))

    init = tf.constant_initializer(value=weights, dtype=tf.float32)
    var = tf.get_variable(name=name, initializer=init, shape=weights.shape)
    return var

def get_correct_deconv_filter_halfch(name, n_channels, k_size):
    """Creates weight kernel initialization for deconvolution layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        name (str): name postfix
        n_channels (int): number of input and output channels are same
        k_size (int): kernel-size (~ 2 x stride for FCN case)
    Returns:
        weight kernels (4-D Tensor): (k_size , k_size, k_size, n_channels, n_channels)
    """
    k = k_size
    f = math.floor((k+1)/2)
    if k%2 == 1:
        cent = f
    else:
        cent = f + 0.5
    C = np.arange(1,k+1)

    filter_shape = [k, k, k, n_channels, n_channels*2]
    weights = np.zeros(filter_shape)
    for i, j in zip(range(int(n_channels)), range(int(n_channels*2))):
        weights[:, :, :, i, j] = np.transpose(np.ones([1,k]) - abs(C-cent)/f)*(np.ones([1,k]) - abs(C-cent)/(f))

    init = tf.constant_initializer(value=weights, dtype=tf.float32)
    var = tf.get_variable(name=name, initializer=init, shape=weights.shape)
    return var

def get_correct_deconv_filter_halfch2d(name, n_channels, k_size):
    """Creates weight kernel initialization for deconvolution layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        name (str): name postfix
        n_channels (int): number of input and output channels are same
        k_size (int): kernel-size (~ 2 x stride for FCN case)
    Returns:
        weight kernels (4-D Tensor): (k_size , k_size, k_size, n_channels, n_channels)
    """
    k = k_size
    f = math.floor((k+1)/2)
    if k%2 == 1:
        cent = f
    else:
        cent = f + 0.5
    C = np.arange(1,k+1)

    filter_shape = [k, k, n_channels, n_channels*2]
    weights = np.zeros(filter_shape)
    for i, j in zip(range(int(n_channels)), range(int(n_channels*2))):
        weights[:, :, i, j] = np.transpose(np.ones([1,k]) - abs(C-cent)/f)*(np.ones([1,k]) - abs(C-cent)/(f))

    init = tf.constant_initializer(value=weights, dtype=tf.float32)
    var = tf.get_variable(name=name, initializer=init, shape=weights.shape)
    return var

def deconv_correct_module(input_, name, stride=2, kernel_size=4, padding='SAME'):
    """ Convolutional transpose layer for upsampling score layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        name (str): name postfix
        stride (int): the upscaling factor (default is 2)
        kernel_size (int): (~ 2 x stride for FCN case)
        padding (str): 'same' or 'valid'
    Returns:
        net: output of transpose convolution operations
    """
    n_channels = input_.get_shape()[4].value
    strides = [1, stride, stride, stride, 1]
    in_shape = input_.get_shape()
    h = in_shape[2].value * stride
    w = in_shape[3].value * stride
    d = in_shape[1].value * stride
    out_shape = tf.stack([in_shape[0].value, d, h, w, n_channels])
    with tf.variable_scope('deconv_{}'.format(name)):
        weights = get_correct_deconv_filter('up_filter_kernel', n_channels, k_size=kernel_size)
        deconv = tf.nn.conv3d_transpose(input_, weights, output_shape=out_shape,
                                        strides=strides, padding=padding)
        return deconv

def deconv_correct_module2d(input_, name, stride=2, kernel_size=4, padding='SAME'):
    """ Convolutional transpose layer for upsampling score layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        name (str): name postfix
        stride (int): the upscaling factor (default is 2)
        kernel_size (int): (~ 2 x stride for FCN case)
        padding (str): 'same' or 'valid'
    Returns:
        net: output of transpose convolution operations
    """
    n_channels = input_.get_shape()[3].value
    strides = [1, stride, stride, 1]
    in_shape = input_.get_shape()
    h = in_shape[1].value * stride
    w = in_shape[2].value * stride
    out_shape = tf.stack([in_shape[0].value, h, w, n_channels])
    with tf.variable_scope('deconv_{}'.format(name)):
        weights = get_correct_deconv_filter2d('up_filter_kernel', n_channels, k_size=kernel_size)
        deconv = tf.nn.conv2d_transpose(input_, weights, output_shape=out_shape,
                                        strides=strides, padding=padding)
        return deconv

def deconv_module(input_, name, stride=2, kernel_size=4, padding='SAME'):
    """ Convolutional transpose layer for upsampling score layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        name (str): name postfix
        stride (int): the upscaling factor (default is 2)
        kernel_size (int): (~ 2 x stride for FCN case)
        padding (str): 'same' or 'valid'
    Returns:
        net: output of transpose convolution operations
    """
    n_channels = input_.get_shape()[4].value
    strides = [1, stride, stride, stride, 1]
    in_shape = input_.get_shape()
    h = in_shape[2].value * stride
    w = in_shape[3].value * stride
    d = in_shape[1].value * stride
    out_shape = tf.stack([in_shape[0].value, d, h, w, n_channels])
    with tf.variable_scope('deconv_{}'.format(name)):
        weights = get_deconv_filter('up_filter_kernel', n_channels, k_size=kernel_size)
        deconv = tf.nn.conv3d_transpose(input_, weights, output_shape=out_shape,
                                        strides=strides, padding=padding)
        return deconv

def deconv_module_halfch(input_, name, stride=2, kernel_size=4, padding='SAME'):
    """ Convolutional transpose layer for upsampling score layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        name (str): name postfix
        stride (int): the upscaling factor (default is 2)
        kernel_size (int): (~ 2 x stride for FCN case)
        padding (str): 'same' or 'valid'
    Returns:
        net: output of transpose convolution operations
    """
    n_channels = input_.get_shape()[4].value // 2
    strides = [1, stride, stride, stride, 1]
    in_shape = input_.get_shape()
    h = in_shape[2].value * stride
    w = in_shape[3].value * stride
    d = in_shape[1].value * stride
    out_shape = tf.stack([in_shape[0].value, d, h, w, n_channels])
    with tf.variable_scope('deconv_{}'.format(name)):
        weights = get_deconv_filter('up_filter_kernel', n_channels, k_size=kernel_size)
        deconv = tf.nn.conv3d_transpose(input_, weights, output_shape=out_shape,
                                        strides=strides, padding=padding)
        return deconv

def deconv_correct_module_halfch(input_, name, stride=2, kernel_size=4, padding='SAME'):
    """ Convolutional transpose layer for upsampling score layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        name (str): name postfix
        stride (int): the upscaling factor (default is 2)
        kernel_size (int): (~ 2 x stride for FCN case)
        padding (str): 'same' or 'valid'
    Returns:
        net: output of transpose convolution operations
    """
    n_channels = input_.get_shape()[4].value // 2
    strides = [1, stride, stride, stride, 1]
    in_shape = input_.get_shape()
    h = in_shape[2].value * stride
    w = in_shape[3].value * stride
    d = in_shape[1].value * stride
    out_shape = tf.stack([in_shape[0].value, d, h, w, n_channels])
    with tf.variable_scope('deconv_{}'.format(name)):
        weights = get_correct_deconv_filter_halfch('up_filter_kernel', n_channels, k_size=kernel_size)
        deconv = tf.nn.conv3d_transpose(input_, weights, output_shape=out_shape,
                                        strides=strides, padding=padding)
        return deconv

def deconv_correct_module_halfch2d(input_, name, stride=2, kernel_size=4, padding='SAME'):
    """ Convolutional transpose layer for upsampling score layer
        reference: https://github.com/MarvinTeichmann/tensorflow-fcn
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        name (str): name postfix
        stride (int): the upscaling factor (default is 2)
        kernel_size (int): (~ 2 x stride for FCN case)
        padding (str): 'same' or 'valid'
    Returns:
        net: output of transpose convolution operations
    """
    n_channels = input_.get_shape()[3].value // 2
    strides = [1, stride, stride, 1]
    in_shape = input_.get_shape()
    h = in_shape[1].value * stride
    w = in_shape[2].value * stride
    out_shape = tf.stack([in_shape[0].value, h, w, n_channels])
    with tf.variable_scope('deconv_{}'.format(name)):
        weights = get_correct_deconv_filter_halfch2d('up_filter_kernel', n_channels, k_size=kernel_size)
        deconv = tf.nn.conv2d_transpose(input_, weights, output_shape=out_shape,
                                        strides=strides, padding=padding)
        return deconv

def global_conv_module2d(input_, num_classes, training, name, k=13, padding='same'):
    """Global convolution network [https://arxiv.org/abs/1703.02719]
    Args:
         input_ (4-D Tensor): (batch_size, H, W, C)
         num_classes (integer) : Number of classes to classify
         name (str): name postfix
         k (integer): filter size for 1 x k + k x 1 convolutions
         padding (str) : 'same' or 'valid
    Returns:
         net (4-D Tensor): (batch_size, H, W, num_classes)
    """
    net = input_
    n_filters = num_classes

    with tf.variable_scope("global_conv_module_{}".format(name)):
        branch_a = tf.layers.conv2d(net, n_filters, (k, 1), activation=None,
                                     padding=padding, name='conv_1a')
        branch_a = tf.layers.conv2d(branch_a, n_filters, (1, k), activation=None,
                                     padding=padding, name='conv_2a')
        
        branch_b = tf.layers.conv2d(net, n_filters, (1, k), activation=None,
                                     padding=padding, name='conv_1b')
        branch_b = tf.layers.conv2d(branch_b, n_filters, (k, 1), activation=None,
                                     padding=padding, name='conv_3b')

        net = tf.add(branch_a, branch_b, name='sum')

        return net

def boundary_refine2d(input_, training, name, activation=tf.nn.relu, batch_norm=True):
    """Boundary refinement network [https://arxiv.org/abs/1703.02719]
    Args:
        input_ (4-D Tensor): (batch_size, H, W, C)
        training (bool): If True, run in training mode
        name (str): name postfix
        activation: Activaion functions
        batch_norm (bool) : Whether to use batch norm
    Returns:
        net (4-D Tensor): output tensor of same shape as input_
    """
    net = input_
    n_filters = input_.get_shape()[3].value

    with tf.variable_scope("boundary_refine_module_{}".format(name)):

        net = tf.layers.conv2d(net, n_filters, (1, 1), activation=None,
                               padding='SAME', name='conv_1')
        if batch_norm:
            net = tf.layers.batch_normalization(net, training=training,
                                                name='bn_1', renorm=True)
        net = activation(net, name='relu_1')

        net = tf.layers.conv2d(net, n_filters, (1, 1), activation=None,
                               padding='SAME', name='conv_2')
        net = tf.add(net, input_, name='sum')

        return net

def attention_module(input_, gating_sig, training, name, batch_norm=True, activation=tf.nn.relu, padding='SAME'):
    net = gating_sig
    gate_dim = net.get_shape()[3].value
    num_conv = int(math.log(net.get_shape()[2].value,2))
    kernel_sizes = []
    n_filters = net.get_shape()[3].value
    print(net.get_shape()[0].value, net.get_shape()[1].value, net.get_shape()[2].value, net.get_shape()[3].value)
    print(input_.get_shape()[0].value, input_.get_shape()[1].value, input_.get_shape()[2].value, input_.get_shape()[3].value)
    print(num_conv)
    for i in range(num_conv):
        kernel_sizes.append(3)
    with tf.variable_scope("conv_att_{}".format(name)):
        for i, k_size in enumerate(kernel_sizes):
            net = tf.layers.conv2d(net, n_filters, (k_size, k_size), activation=None, padding=padding,
                                   name="conv_{}".format(i + 1))
            n_filters *= 2
            if batch_norm:
                net = tf.layers.batch_normalization(net, training=training, renorm=True,
                                                    name="bn_{}".format(i + 1))
            net = activation(net, name="relu_{}".format(i + 1))
            net = tf.layers.max_pooling2d(net, (2, 2), strides=(2, 2), name="pool")
    img_dim = input_.get_shape()
    print(net.get_shape()[0].value, net.get_shape()[1].value, net.get_shape()[2].value, net.get_shape()[3].value)
    add_sig = tf.tile(net,[1,img_dim[1].value,img_dim[2].value,1])
    add_input = tf.layers.conv2d(input_, add_sig.get_shape()[3].value, (1,1), activation=None, padding=padding,
                                   name="input_add_{}".format(name))
    added_signal = tf.add(add_input,add_sig,name="added_{}".format(name))
    add_filtered = tf.layers.conv2d(added_signal, 1, (1,1), activation=None, padding=padding,
                                   name="filter_add_{}".format(name))
    add_norm = tf.layers.batch_normalization(add_filtered,training=training, name="bn_{}".format(name))
    mul_sig = tf.tile(add_norm,[1,1,1,img_dim[3].value])
    mul_data = tf.multiply(input_,mul_sig)
    return mul_data    


def add_coordconv_module(input_, with_r):
    batch_size_tensor = input_.get_shape()[0].value
    x_dim = input_.get_shape()[1].value
    y_dim = input_.get_shape()[2].value
    xx_ones = tf.ones([batch_size_tensor, x_dim],
                    dtype=tf.int32)
    xx_ones = tf.expand_dims(xx_ones, -1)
    xx_range = tf.tile(tf.expand_dims(tf.range(y_dim), 0),
                      [batch_size_tensor, 1])
    xx_range = tf.expand_dims(xx_range, 1)
    xx_channel = tf.matmul(xx_ones, xx_range)
    xx_channel = tf.expand_dims(xx_channel, -1)
    yy_ones = tf.ones([batch_size_tensor, y_dim],
                dtype=tf.int32)
    yy_ones = tf.expand_dims(yy_ones, 1)
    yy_range = tf.tile(tf.expand_dims(tf.range(x_dim), 0),
                  [batch_size_tensor, 1])
    yy_range = tf.expand_dims(yy_range, -1)
    yy_channel = tf.matmul(yy_range, yy_ones)
    yy_channel = tf.expand_dims(yy_channel, -1)
    xx_channel = tf.cast(xx_channel, 'float32')/(x_dim - 1)
    yy_channel = tf.cast(yy_channel, 'float32')/(y_dim - 1)
    xx_channel = xx_channel*2 - 1
    yy_channel = yy_channel*2 - 1
    ret = tf.concat([input_,xx_channel,yy_channel], axis=-1)
    if with_r:
        rr = tf.sqrt( tf.square(xx_channel) + tf.square(yy_channel))
        ret = tf.concat([ret,rr],axis=-1)
    return ret



